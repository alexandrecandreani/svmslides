exclude=*.ps *.lof *.lot *~ *.gls *.glsdefs  *.aux  *.nlo *.glo  \
*.blg *.log  *.out *.bak* *.bbl *.toc *.glg *.ind *.nls *.idx  *.ilg *.xdy *.nav *.snm *.vrb 
all:	
	make mess
mess:
	if [ ! -f "./m.mp4" ] ; then  \
                wget -N -O m.mp4 "https://www.dropbox.com/s/9w4xkxbqxbavi3t/m.mp4" ;\
        fi
	if [ ! -f "./VPlayer9.swf" ] ; then  \
                wget -N -O VPlayer9.swf "https://www.dropbox.com/s/uh70fwosi6seczu/VPlayer9.swf" ;\
        fi

	
	pdflatex  -src -interaction=nonstopmode main.tex
	bibtex main.aux
#	makeindex main.idx
#	makeindex main.nlo -s nomencl.ist -o main.nls
#	makeglossaries main
	pdflatex   -src -interaction=nonstopmode main.tex
	make main
main:
	pdflatex  -src -interaction=nonstopmode main.tex 
		
clean:
	rm -fr **/*.lof **/*.lot **/*~ **/*.aux **/*.dvi \
	**/*.bbl **/*.blg **/*.log **/*.ps **/*.pdf **/*.out **/*.bak* **/*.toc **/*.nlo **/*.glo
	rm -fr $(exclude) 

cleanall:
	rm -fr **/*.lof **/*.lot **/*~ **/*.aux **/*.dvi \
	**/*.bbl **/*.blg **/*.log **/*.ps **/*.pdf **/*.out **/*.bak* **/*.toc **/*.nlo **/*.glo
	rm -fr $(exclude) *.pdf

update:
	make cleanall
	git add -A
	git reset -- m.mp4 
	git reset -- VPlayer9.swf
	echo "comente sua atualizacao";  \
	read comment; \
	git commit -am "$$comment"
	git push -u origin master
